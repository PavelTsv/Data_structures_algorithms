#pragma once
#include <vector>
#include <algorithm>

namespace alg
{
	namespace sort
	{
		template<typename T>
		static void insertion_sort(T* arr, const size_t size)
		{
			for (auto j = 1; j < size; j++)
			{
				for (auto i = j - 1; i >= 0 && arr[i] > arr[i + 1]; --i)
				{
					std::swap(arr[i], arr[i + 1]);
				}
			}
		}
		
		template<typename T>
		static void insertion_sort(std::vector<T>& vector)
		{
			for (auto it = vector.begin(), end = vector.end(); it != end; ++it)
			{
				auto const insertion_point = std::upper_bound(vector.begin(), it, *it);

				std::rotate(insertion_point, it, it + 1);
			}
		}

	}
}
