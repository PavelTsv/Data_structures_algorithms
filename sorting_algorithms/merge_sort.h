﻿#pragma once
#include <vector>
#include <cassert>
#include <algorithm>

namespace alg
{
	namespace sort
	{
		template <typename T>
		static void merge(T* arr, const unsigned int left, const unsigned int mid, const unsigned int right)
		{
			assert(arr != nullptr && "The pointer points at nothing.");
			assert(left <= mid && mid <= right && "Incorrect indexes.");

			unsigned int position = 0;
			auto left_position = left;
			auto right_position = mid + 1;
			T* temp = new T[right - left + 1];

			while (left_position <= mid && right_position <= right)
			{
				if (arr[left_position] <= arr[right_position])
				{
					temp[position++] = arr[left_position++];
				}
				else
				{
					temp[position++] = arr[right_position++];
				}
			}

			while (left_position <= mid)
			{
				temp[position++] = arr[left_position++];
			}
			while (right_position <= right)
			{
				temp[position++] = arr[right_position++];
			}

			for (auto i = 0; i < position; i++)
			{
				arr[left + i] = temp[i];
			}

			delete[] temp;
		}

		template <typename T>
		static void merge_sort(T* arr, const unsigned int left,
			const unsigned int right)
		{
			const auto mid = left + (right - left) / 2;

			if (left < right)
			{
				merge_sort(arr, left, mid);
				merge_sort(arr, mid + 1, right);
				merge(arr, left, mid, right);
			}

		}

		template <typename Iter>
		static void merge_sort(Iter first, Iter last)
		{
			if (last - first > 1)
			{
				Iter mid = first + (last - first) / 2;
				merge_sort(first, mid);
				merge_sort(mid, last);
				std::inplace_merge(first, mid, last);
			}
		}

		template <typename T>
		static void merge_sort(std::vector<T>& vector)
		{
			merge_sort(vector.begin(), vector.end());
		}
	}
}


