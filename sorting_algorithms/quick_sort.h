#pragma once

namespace alg
{
	namespace sort
	{
		template<typename T>
		static void quick_sort(T* arr, const size_t low, const size_t high)
		{
			if (low < high)
			{
				auto partition_index = partition(arr, low, high);

				quick_sort(arr, low, partition_index - 1);
				quick_sort(arr, partition_index + 1, high);
			}
		}

		template<typename T>
		static unsigned int partition(T* arr, const size_t low, const size_t high)
		{
			auto const pivot = arr[high];
			auto index_smaller_element = low - 1;

			for (auto j = low; j < high; j++)
			{
				if (arr[j] <= pivot)
				{
					std::swap(arr[++index_smaller_element], arr[j]);
				}
			}
			std::swap(arr[index_smaller_element + 1], arr[high]);
			return index_smaller_element + 1;
		}
	}
}