#pragma once
#include <vector>
#include <algorithm>

namespace alg
{
	namespace sort
	{
		template<typename T>
		static void selection_sort(T* arr, const size_t size)
		{
			for (auto i = 0; i < size; i++)
			{
				auto* smallest = arr + i;

				for (auto j = i + 1; j < size; j++)
				{
					if (arr[j] < *smallest)
					{
						smallest = arr + j;
					}
				}

				std::swap(arr[i], *smallest);

				delete smallest;
			}
		}

		template<typename T>
		static void selection_sort(std::vector<T>& vector)
		{
			for (auto i = 0; i < vector.size(); i++)
			{
				std::iter_swap(std::min_element(vector.begin() + i, vector.end()), vector.begin() + i);
			}
		}
	}
}
