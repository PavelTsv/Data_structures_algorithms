#pragma once

namespace alg
{
	namespace search
	{
		template<typename T>
		T* binary_search(T* start_arr, T* end_arr, const T element)
		{
			if (start_arr <= end_arr)
			{
				const auto mid = (end_arr - start_arr) / 2;

				if (start_arr[mid] == element)
				{
					return start_arr + mid;
				}

				if (element < start_arr[mid])
				{
					return binary_search(start_arr, start_arr + mid - 1, element);
				}

				return binary_search(start_arr + mid + 1, end_arr, element);
			}

			return nullptr;
		}

		template<typename T>
		T binary_search(T* arr, const size_t left, const size_t right, const T element)
		{
			if (left <= right)
			{
				const auto mid = left + (right - left) / 2;

				if (element == arr[mid])
				{
					return mid;
				}

				if (element < arr[mid])
				{
					return binary_search(arr, 0, mid - 1, element);
				}

				return binary_search(arr, mid + 1, right, element);
			}
			return -999;
		}

		template<typename T>
		T binary_search_no_r(T* arr, size_t left, size_t right, const T element)
		{
			while (right - left > 1)
			{
				const auto mid = left + (right - left) / 2;

				if (arr[mid] <= element)
					left = mid;
				else
					right = mid;
			}
			if (arr[left] == element)
				return left;

			return -1;
		}

	}
}
