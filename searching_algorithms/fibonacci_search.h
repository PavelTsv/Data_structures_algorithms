#pragma once

namespace alg
{
	namespace search
	{
		int find_smallest_fib_num(const int number)
		{
			auto first_fib = 0;
			auto second_fib = 1;
			auto fib_num = first_fib + second_fib;

			while (fib_num < number)
			{
				first_fib = second_fib;
				second_fib = fib_num;
				fib_num = first_fib + second_fib;

			}

			return fib_num;
		}

		int fibonacci_search(int* arr, const unsigned int size, const int element)
		{
			const auto num_greater_than_size = find_smallest_fib_num(size);

			return 0;
		}
	}
}