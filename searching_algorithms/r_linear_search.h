#pragma once

namespace alg
{
	namespace search
	{
		template<typename T>
		T* r_linear_search(const T* const arr, const T* const end_arr, const T element)
		{
			if (end_arr < arr)
				return nullptr;
			if (*arr == element)
				return arr;
			return r_linear_search(arr + 1, end_arr, element);
		}
	}
}