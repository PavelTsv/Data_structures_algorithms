#pragma once
#include <vector>
#include <algorithm>

namespace alg
{
	namespace sort
	{
		template<typename T>
		T jump_search(T* arr, const size_t size, const T element)
		{
			size_t step = sqrt(size);
			auto prev = 0;

			while (arr[std::min(step, size) - 1] < element)
			{
				prev = step;
				step += step;

				if (prev >= size)
					return -1;
			}

			return binary_search(arr, prev, std::min(step, size), element);
		}

		template<typename T>
		T jump_search(std::vector<T>& vector, const T element)
		{
			if (vector.size() == 0)
				return -1;


			size_t step = sqrt(vector.size());
			auto prev = 0;

			while (vector[std::min(step, vector.size()) - 1] < element)
			{
				prev = step;
				step += step;

				if (prev >= vector.size())
					return -1;
			}
			return -1;
		}

	}
}
