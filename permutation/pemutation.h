#pragma once
#include <string>
#include <iostream>
#include <algorithm>

namespace alg
{
	inline void permute(const std::string& str)
	{
		std::sort(str.begin(), str.end());

		do
		{
			std::cout << str << std::endl;
		} while (std::next_permutation(str.begin(), str.end()));
	}

	void permute_r(std::string& str, const size_t left, const size_t right)
	{
		if (left == right)
		{
			std::cout << str << "\n";
		}
		else
		{
			for (auto i = left; i <= right; i++)
			{
				std::swap(str[left], str[i]);
				permute_r(str, left + 1, right);
				std::swap(str[left], str[i]);
			}
		}
	}

	void permute_r(std::string& str)
	{
		permute_r(str, 0, str.size() - 1);
	}



}
