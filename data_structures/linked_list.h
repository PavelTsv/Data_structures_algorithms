﻿#pragma once

template<typename T>
class linked_list
{
	template<typename G = T>
	struct node
	{
		T item;
		node<T>* next;
	};

	node<T>* front_;
	node<T>* rear_;
	unsigned int count_ = 0;

public:
	linked_list();
	~linked_list();
	void insert_front(const T item);
	void insert_rear(const T item);
	void print_list() const;
};
