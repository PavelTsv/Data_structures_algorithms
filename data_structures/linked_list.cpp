﻿#include "stdafx.h"
#include "linked_list.h"
#include <iostream>

template <typename T>
linked_list<T>::linked_list()
{
	front_ = nullptr;
	rear_ = nullptr;
}

template <typename T>
linked_list<T>::~linked_list()
{
	
}

template <typename T>
void linked_list<T>::insert_front(const T item)
{
	if (front_ == nullptr)
	{
		front_ = new node<T>();
		front_->item_ = item;
		front_->next_ = nullptr;
		rear_ = new node<T>();
		rear_ = front_;
	} 
	else
	{
		node<T>* new_node = new node<T>();
		new_node->item_ = item;
		new_node->next_ = nullptr;
		front_ = new_node;
	}
}

template <typename T>
void linked_list<T>::insert_rear(const T item)
{
	if (rear_ == nullptr)
	{
		front_ = new node<T>();
		front_->item_ = item;
		front_->next_ = nullptr;
		rear_ = new node<T>();
		rear_ = front_;
	}
	else
	{
		node<T>* new_node = new node<T>();
		new_node->item_ = item;
		rear_->next_ = new_node;
		rear_ = new_node;
	}
}

template <typename T>
void linked_list<T>::print_list() const
{
	node<T> *  temp = front_;
	std::cout << front_->next_ << std::endl;
	while (temp->next_ != nullptr)
	{
		std::cout << " " << temp->item_ << "";
		if (temp != nullptr)
		{
			temp = (temp->next_);
		}
		else
		{
			break;
		}
	}
}

template class linked_list<int>;